package com.techu.apitechuejemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechuejemploApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechuejemploApplication.class, args);
	}

}
