package com.techu.apitechu2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apitechu2Application {

	public static void main(String[] args) {
		SpringApplication.run(Apitechu2Application.class, args);
	}

}
