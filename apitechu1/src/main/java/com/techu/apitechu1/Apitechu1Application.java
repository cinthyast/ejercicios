package com.techu.apitechu1;

import com.techu.apitechu1.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
public class Apitechu1Application {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(Apitechu1Application.class, args);

		Apitechu1Application.productModels = Apitechu1Application.getTestData();
	}

	private static ArrayList<ProductModel> getTestData()
	{
		ArrayList<ProductModel> productModels = new ArrayList<>();
		productModels.add(
				new ProductModel(
					"1",
					"Producto 1",
					10
				)
		);
		productModels.add(
				new ProductModel(
						"2",
						"Producto 2",
						20
				)
		);
		productModels.add(
				new ProductModel(
						"3",
						"Producto 3",
						30
				)
		);

		return productModels;
	}

}
