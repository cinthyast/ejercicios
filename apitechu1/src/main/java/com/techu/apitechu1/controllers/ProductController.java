package com.techu.apitechu1.controllers;
import com.techu.apitechu1.Apitechu1Application;
import com.techu.apitechu1.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String API_BASE_URL = "/apitechu1/v1";

    @GetMapping(API_BASE_URL + "/products")
    public ArrayList<ProductModel>getProducts(){
        System.out.println("getProducts");

        return Apitechu1Application.productModels;

    }

    @GetMapping(API_BASE_URL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id)
    {
        System.out.println("getProductsById");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();
        for (ProductModel product: Apitechu1Application.productModels)
        {
                if(product.getId().equals(id))
                {
                    result = product;
                }
         }

        return result;
    }

    @PostMapping(API_BASE_URL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct)
    {
        System.out.println("createProduct");
        System.out.println("la id del nuevo productos es " + newProduct.getId());
        System.out.println("la descripción del nuevo productos es " + newProduct.getDesc());
        System.out.println("el precio del nuevo productos es " + newProduct.getPrice());

        Apitechu1Application.productModels.add(newProduct);

        return newProduct;

    }

    @PutMapping(API_BASE_URL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product,@PathVariable String id)
    {
        System.out.println("updateProduct");
        System.out.println("la id del producto a actualizar en parametro URL es " + id);
        System.out.println("la id del producto a actualizar es " + product.getId());
        System.out.println("la descripción del producto a actualizar es " + product.getDesc());
        System.out.println("el precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : Apitechu1Application.productModels)
        {
            if(productInList.getId().equals(id))
            {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;

    }

    @DeleteMapping(API_BASE_URL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id)
    {
        System.out.println("deleteProduct");
        System.out.println("la id del producto a borrar es " + id);
      //  System.out.println("la descripción del producto a borrar es " + product.getDesc());
      //  System.out.println("el precio del producto a borrar es " + product.getPrice());

        ProductModel result = new ProductModel();
        boolean foundProduct = false;

        for (ProductModel productInList : Apitechu1Application.productModels)
        {
            if(productInList.getId().equals(id))
            {
                System.out.println("Producto a borrar encontrado " + id);
                foundProduct = true;
                result = productInList;
            }
        }
        if (foundProduct)
        {
            System.out.println("Borrando producto "+ id);
            Apitechu1Application.productModels.remove(result);
        }
        else{
            System.out.println("No se ha encontrado producto a borrar "+ id);
        }

        return new ProductModel();
    }

    //Actualización parcial, debe actualizar el precio y/o descripción. Tiene que comprobar que llegue el elemento a actualizar. O que se envia los dos. Precio y Descripción.
    @PatchMapping(API_BASE_URL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel product,@PathVariable String id)
    {
        System.out.println("updatePatchProduct");
        System.out.println("la id del producto a actualizar en parametro URL es " + id);

        ProductModel result = new ProductModel();
        boolean patch_product = false;


        for (ProductModel productInList : Apitechu1Application.productModels)
        {
            if(productInList.getId().equals(id))
            {
                System.out.println("Producto Id encontrado " + id);
                if (productInList.getDesc() !=  null)
                {
                    System.out.println("Producto Descripción encontrado " + product.getDesc());
                    productInList.setDesc(product.getDesc());
                }
                if (productInList.getPrice() > 0)
                {
                    System.out.println("Producto Price encontrado " + product.getPrice());
                    productInList.setPrice(product.getPrice());
                }
                if (product.getId()!= null)
                {
                    System.out.println("El producto no puede actualizarse");
                }
                result = productInList;

            }
        }

        if (patch_product)
        {
            System.out.println("Se ha actualizado el producto");
        }

        return result;

    }
}