package com.techu.apitechudb1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apitechudb1Application {

	public static void main(String[] args) {
		SpringApplication.run(Apitechudb1Application.class, args);
	}

}
